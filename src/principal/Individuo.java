/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;
import abstratas.AIndividuo;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;
import tools.CFG;

/**
 *
 * @author gomes
 */
public class Individuo extends AIndividuo{
    public float cromossomo[];
    public int temp_id;
    public double f_furo, f_status, f_tempo;

    public Individuo() {
        super();
        cromossomo = new float[CFG.I_QTD_GENES];
        fitness = 0f;
        this.f_furo = 0f;
        this.f_status = 0f;
        this.f_tempo = 0f;
    }
    
    @Override
    public void inicializa() {
        for (int gene = 0; gene < cromossomo.length; gene++) {
            // Inicializa individuo de forma randomica
            if (gene%3 == 2) {
                //cromossomo[gene] = AG.getNextFloat(false) * CFG.I_INTERVALO_CHEGADA; //RMV-07.12:RETIRADA-TEMPO
                cromossomo[gene] = 0f;
            }else {
                //cromossomo[gene] = AG.getNextFloat(true);
                cromossomo[gene] = getTxInicial(0.3f, 0.25f);
            }
        }
    }

    @Override
    public int getGrauSemelhanca(AIndividuo comparado) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AIndividuo getCopia() {
        Individuo copia = new Individuo();
        copia.fitness = this.fitness;
        copia.idade = this.idade;
        System.arraycopy(this.cromossomo, 0, copia.cromossomo, 0, copia.cromossomo.length);
        
        return copia;
    }
    
    @Override
    public String toString(){
    NumberFormat format_taxa = new DecimalFormat("0.000"); 
    NumberFormat format_tempo = new DecimalFormat("00.00"); 
    
        String temp = "[" + format_taxa.format(cromossomo[0]);
        for (int i = 1; i < cromossomo.length; i++) {
            if (i%3==2){
                temp += " " + format_tempo.format(cromossomo[i]);
            }else{
                temp += " " + format_taxa.format(cromossomo[i]);
            }
        }
        temp += "]{"+super.idade+", "+ format_taxa.format(super.fitness) + "}";
        return temp;
    }
    
    public float getMediaTxFuro(){
        float media = 0f;
        for (int i = 0; i < 5; i++) {
            media += cromossomo[i*3];
        }
        
        return media/5;
    }
    
    public float getMediaTxReclame(){
        float media = 0f;
        for (int i = 0; i < 5; i++) {
            media += cromossomo[(i*3)+1];
        }
        
        return media/5;
    }
    
    public float getTxFuroDia(int dia){
        if (dia < 0 || dia > CFG.FILA_DIAS_POR_RODADA) {
            System.out.println("ERRO NO DIA INFORMADO!");
            return-1f;
        }else{
            return cromossomo[dia*3];
        }
    }
    
    public float getTxReclameDia(int dia){
        if (dia < 0 || dia > CFG.FILA_DIAS_POR_RODADA) {
            System.out.println("ERRO NO DIA INFORMADO!");
            return-1f;
        }else{
            return cromossomo[dia*3 +1];
        }
    }
    
    private float getTxInicial(float media, float desvPadrao){
        Random rnd = new Random();
        float newTx = (float) (rnd.nextGaussian() * desvPadrao + media);
        if (newTx < 0){
            newTx = 0;
        }else if(newTx > 1){
            newTx = 1;
        }
        return newTx;
    }

    @Override
    public void calculaAtributos() {
        super.setObjective(0, f_tempo);
        super.setObjective(1, f_status+f_furo);
        
        System.out.println("{Obj1: " + super.getObjective(0) + ", Obj2: " +super.getObjective(1) + "}");
    }
    
}

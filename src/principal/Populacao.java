/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import abstratas.*;
import tools.CFG;

/**
 *
 * @author gomes
 */
public class Populacao extends APopulacao{
    private float mediaTxFuro;
    private float mediaTxReclame;

    public Populacao(int qtd_individuos) {
        super(qtd_individuos);
        individuos = new Individuo[qtd_individuos];
    }
    
    public void inicializaPopulacao(){
        for (int i =0; i < individuos.length; i++) {
            individuos[i] = new Individuo();
            individuos[i].inicializa();
        }
    }
    
    public void calculaMedidas(){
        float somaFitness = 0;
        for (AIndividuo individuo : individuos) {
            somaFitness += individuo.fitness;
        }
        media = somaFitness / individuos.length;
        // ************************************************************ @DEBUG{
        if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.print("Media: " + media);
        // ************************************************************ @DEBUG}
        float variancia = 0f;
        for (AIndividuo individuo : individuos) {
            variancia += Math.pow(individuo.fitness - media, 2);
        }
        variancia /= individuos.length;
        desvioPadrao = (float) Math.sqrt(variancia);
        // ************************************************************ @DEBUG{
        if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println(", DesvioPadrao: " + desvioPadrao);
        // ************************************************************ @DEBUG}
        
        // Calcula medidas das taxas
        Individuo l_indiv;
        float somaTxFuro = 0f, somaTxReclame = 0f;
        for (AIndividuo individuo : individuos) {
            l_indiv = (Individuo) individuo;
            somaTxFuro += l_indiv.getMediaTxFuro();
            somaTxReclame += l_indiv.getMediaTxReclame();
        }
        
        
        mediaTxFuro = somaTxFuro / individuos.length;
        mediaTxReclame = somaTxReclame / individuos.length;
    }
    
    @Override
    public String toString(){
        String temp = "";
        for (int i = 0; i < individuos.length; i++) {
            if (i%5 == 0){
                temp += "\n";
            }
            temp += individuos[i].toString();
            temp += " ";
        }
              
        return temp;
    }

    public float getMediaTxFuro() {
        return mediaTxFuro;
    }

    public float getMediaTxReclame() {
        return mediaTxReclame;
    }
    
}

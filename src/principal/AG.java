/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import abstratas.AAG;
import abstratas.AIndividuo;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import tools.CFG;
import tools.Fila;
import tools.MersenneTwisterFast;
import tools.LogAG;
import tools.QuickSort;
import tools.Simulacao;

/**
 *
 * @author gomes
 */
public class AG extends AAG {

    private static final MersenneTwisterFast twister = new MersenneTwisterFast();
    Populacao populacao;
    private final boolean CONFIG_FTNS_GRUPO;
    private int cont_iteracoesAposMelhorFitnessGlobal = 0;
    private float ultimoMelhorFitness = 0f;
    private float melhorFitnessAnterior = 0f;

    private float taxa_crossover = 0.5f, taxa_mutacao = 0.2f;
    private List<Boolean> ultimasCincoEvolucoes;

    private final LogAG log;
    private final String nomeExecucao;

    List<AIndividuo> pop_temp;
    private Fila fila;
    
    private float pontoDeReferencia = 0f;
    private int cont_fitDentroFaixa = 0;

    public AG(String nome, boolean fitness_de_grupo) {
        this.nomeExecucao = nome;
        this.log = new LogAG(nome);
        this.CONFIG_FTNS_GRUPO = fitness_de_grupo;
        this.fila = new Fila(fitness_de_grupo);

        this.ultimasCincoEvolucoes = new ArrayList<>();
    }

    public static float getNextFloat(boolean includeOne) {
        return twister.nextFloat(true, includeOne);
    }

    public static void main(String args[]) {
        /*
         Individuo ind = new Individuo();
         ind.inicializa();
         System.out.println(ind.toString());
         */
        
        /*
        AG teste_individual = new AG("t_indv", false);
        AG teste_grupo = new AG("t_grupo", true);
        
        teste_individual.inicia();
        System.out.println("\n############################################################");
        System.out.println("############################################################");
        System.out.println("############################################################");
        teste_grupo.inicia();
        
        
        */
        // Fitness individual
        /*
        AG[] ag = new AG[5];
        for (int i = 0; i < ag.length; i++) {
            ag [i] = new AG("FilaRU_INDV_"+i, false);
            ag [i].inicia();
        }
        */
        
        
        AG[] ag_grupo = new AG[5];
        for (int i = 0; i < ag_grupo.length; i++) {
            ag_grupo [i] = new AG("FilaRU_GRUP_"+i, true);
            ag_grupo [i].inicia();
        }
        //
        
    }

    private boolean atingiuCondicaoParada(){
        if (this.CONFIG_FTNS_GRUPO){
            if (cont_iteracoesSemEvoluir >= CFG.AG_CP_MAX_ITERACOES_SEM_EVOLUCAO){
                return true;
            } else{
                return false;
            }
        }else{
            if (cont_fitDentroFaixa >= 4 * 12 && super.num_iteracao >= 4*12){
                return true;
            }else{
                return false;
            }
            
        }
    }
    
    public void inicia() {

        populacao = new Populacao(CFG.P_QTD_POPULACAO);
        populacao.inicializaPopulacao();
//super.num_iteracao < CFG.AG_CP_MAX_ITERACOES && 
        while ( !atingiuCondicaoParada() ) {
            if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("Geracao:" + num_iteracao + ", ISE: " + cont_iteracoesAposMelhorFitnessGlobal + ", M: " + populacao.getMedia());
            if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("\tTx.Cross: " + taxa_crossover + ", Tx.Mut: " + taxa_mutacao + ", Melhor F: " + populacao.getMelhorIndividuo().fitness);
            AIndividuo[] pais = selecionaPais(populacao);
            AIndividuo[] filhos = reproduz(pais);

            populacao.setIndividuos(aplicaRichthofen(pais, filhos));

            calculaFitness(populacao);
            populacao.calculaMedidas();

            num_iteracao++;

            // Atualiza media e contador de iteracoes sem evolucao
            if (populacao.getMedia() > ultimoMelhorFitness) {
                cont_iteracoesAposMelhorFitnessGlobal = 0;
            } else {
                cont_iteracoesAposMelhorFitnessGlobal++;
            }
            ultimoMelhorFitness = populacao.getMedia();

            // Melhor individuo
            if (populacao.getMelhorIndividuo().fitness > melhorFitnessAnterior) {
                cont_iteracoesSemEvoluir = 0;
            } else {
                cont_iteracoesSemEvoluir++;
            }
            melhorFitnessAnterior = populacao.getMelhorIndividuo().fitness;
            
            // Faixa de melhora
            if (Math.abs(melhorFitnessAnterior-pontoDeReferencia) < CFG.AG_MAX_FAIXA_FITNESS ){
                cont_fitDentroFaixa++;
            } else{
                //System.out.println(Math.abs(melhorFitnessAnterior-pontoDeReferencia) );
                pontoDeReferencia = melhorFitnessAnterior;
                cont_fitDentroFaixa = 0;
            }
            
            
            aplicaRechenberg();
            // Grava dados da iteracao no arquivo .csv
            log.gravaIteracao(this, populacao);
            
            //System.out.println(populacao.toString());
            //System.out.println("Geracao:" + num_iteracao + ", ISE: " + cont_iteracoesAposMelhorFitnessGlobal + ", M: " + populacao.getMedia() + ", MTF: " + populacao.getMediaTxFuro()
            //+ ", MTR: " +populacao.getMediaTxReclame());
            //System.out.println("\tTx.Cross: " + taxa_crossover + ", Tx.Mut: " + taxa_mutacao + ", Melhor F: " + populacao.getMelhorIndividuo().fitness);
        }
        log.encerra(populacao.getMelhorIndividuo().toString());
        System.out.println("\n\t!EXECUCAO: " + nomeExecucao);
        //System.out.println(populacao.toString());
        System.out.println("Geracao:" + num_iteracao + ", ISE: " + cont_iteracoesAposMelhorFitnessGlobal + ", M: " + populacao.getMedia() + ", MTF: " + populacao.getMediaTxFuro()
        + ", MTR: " +populacao.getMediaTxReclame());
        System.out.println("\tTx.Cross: " + taxa_crossover + ", Tx.Mut: " + taxa_mutacao + ", Melhor F: " + populacao.getMelhorIndividuo().fitness);
    }

    private AIndividuo[] selecionaPais(Populacao populacao) {
        pop_temp = new ArrayList<>();

        List<Integer> listaIndexPais = new ArrayList<>();

        AIndividuo l_individuos[] = populacao.getIndividuos();
        // Método: Torneio
        AIndividuo listaDePais[] = new Individuo[CFG.AG_SS_QTD_FILHOS];
        for (int i = 0; i < listaDePais.length; i++) {
            listaDePais[i] = new Individuo();
        }
        int rnd = 0;
        for (int i = 0; i < CFG.AG_SS_QTD_FILHOS; i++) {
            // Aleatorio na metade inferior
            rnd = l_individuos.length/2 + twister.nextInt(l_individuos.length/2) ;
            pop_temp.add(l_individuos[rnd].getCopia() );
        }

        int cont_PaisSelecionados = 0;

        while (cont_PaisSelecionados < CFG.AG_SS_QTD_FILHOS) {
            // 1. Seleciona uma porção dos individuos

            int index_melhorIndiviuo = -1;
            for (int sorteado = 0; sorteado < CFG.AG_TORN_QTD_INDIV_RINGUE; sorteado++) {
                // Verifica qual melhor
                rnd = twister.nextInt(CFG.P_QTD_POPULACAO);

                if (sorteado > 0) {

                    if (l_individuos[rnd].fitness > l_individuos[index_melhorIndiviuo].fitness) {
                        index_melhorIndiviuo = rnd;
                    }
                } else {
                    index_melhorIndiviuo = rnd;
                }
            }

            // Adiciona index a lista index de pais
            boolean jaEstaAdicionado = false;
            for (Integer listaIndexPai : listaIndexPais) {
                if (listaIndexPai == index_melhorIndiviuo) {
                    jaEstaAdicionado = true;
                    break;
                }
            }
            if (!jaEstaAdicionado) {
                listaIndexPais.add(index_melhorIndiviuo);
            }
            // ###################################

            listaDePais[cont_PaisSelecionados] = (Individuo) l_individuos[index_melhorIndiviuo].getCopia();
            // listaDePais[cont_PaisSelecionados].idade += 1; //Removido 07/12

            cont_PaisSelecionados++;
        }

        /* REMOVIDO 07/12
        for (Integer listaIndexPai : listaIndexPais) {
            l_individuos[listaIndexPai] = null;
        }
        for (int i = 0; i < l_individuos.length; i++) {
            if (l_individuos[i] != null) {
                pop_temp.add(l_individuos[i].getCopia());
            }
        }*/

        return listaDePais;
    }

    private AIndividuo[] reproduz(AIndividuo[] paisSelecionados) {
        Individuo[] pais = (Individuo[]) paisSelecionados;

        // 1. Crossover (n-point)
        Individuo[] filhos = new Individuo[paisSelecionados.length]; // Pais e filhos :}
        int l_pontoDeCorte;

        for (int indiv = 0; indiv < pais.length; indiv += 2) {

            // Se houver crossover
            if (twister.nextFloat() <= taxa_crossover) {
            //if (true) {
                l_pontoDeCorte = 1 + twister.nextInt(CFG.I_QTD_GENES / 3 - 1);
                l_pontoDeCorte *= 3;
                Individuo filho1 = new Individuo(), filho2 = new Individuo();

                for (int i = 0; i < CFG.I_QTD_GENES; i++) {
                    if (i < l_pontoDeCorte) {
                        filho1.cromossomo[i] = pais[indiv].cromossomo[i];
                        filho2.cromossomo[i] = pais[indiv + 1].cromossomo[i];
                    } else {
                        filho1.cromossomo[i] = pais[indiv + 1].cromossomo[i];
                        filho2.cromossomo[i] = pais[indiv].cromossomo[i];
                    }
                    // 2. Mutacao (randomiza) ###################################
                    if (twister.nextDouble() <= taxa_mutacao) {
                        if (i % 3 == 2) {
                            //filho1.cromossomo[i] = twister.nextFloat(true, true) * CFG.I_INTERVALO_CHEGADA; //RMV-07.12:RETIRADA-TEMPO
                            filho1.cromossomo[i] = 0f;  
                        } else {
                            //filho1.cromossomo[i] = twister.nextFloat(true, true);
                            filho1.cromossomo[i] = getTxMutation(filho1.cromossomo[i]);
                        }
                    }
                    if (twister.nextDouble() <= taxa_mutacao) {
                        if (i % 3 == 2) {
                            //filho2.cromossomo[i] = twister.nextFloat(true, true) * CFG.I_INTERVALO_CHEGADA; //RMV-07.12:RETIRADA-TEMPO
                            filho2.cromossomo[i] = 0f;
                        } else {
                            //filho2.cromossomo[i] = twister.nextFloat(true, true);
                            filho2.cromossomo[i] = getTxMutation(filho2.cromossomo[i]);
                        }
                    }
                    //---------------------------###################################
                    if (i % 3 == 1){ //Para salta alelo do horario
                        i++;
                    }
                }

                filhos[indiv] = filho1;
                filhos[indiv + 1] = filho2;
            }
        }

        return filhos;
    }

    private AIndividuo[] aplicaRichthofen(AIndividuo[] pais, AIndividuo[] filhos) {

        Individuo[] sobreviventes;
        sobreviventes = new Individuo[CFG.P_QTD_POPULACAO];
        int adicionados = 0;
        
        // Adiciona parcela com melhor fitness
        for (adicionados=0; adicionados<CFG.P_QTD_POPULACAO-CFG.AG_SS_QTD_FILHOS; adicionados++){
            sobreviventes[adicionados] = (Individuo) populacao.getIndividuo(adicionados);
            sobreviventes[adicionados].idade++;
        }
        
        // Adiciona filhos
        for (int i = 0; i < filhos.length; i++) {
            if (twister.nextFloat() <= CFG.AG_MR_TX_MATAR_PAI && filhos[i] != null) {
                sobreviventes[adicionados] = (Individuo) filhos[i];
            } else {
                sobreviventes[adicionados] = (Individuo) pop_temp.get(0);
                sobreviventes[adicionados].idade++;
            }
            pop_temp.remove(0);
            adicionados++;
        }

        /* REMOVIDO 07/12
        for (int intactos = i; intactos < CFG.P_QTD_POPULACAO; intactos++) {
            sobreviventes[intactos] = (Individuo) pop_temp.get(0);
            pop_temp.remove(0);
        }
        */
        /*
         // Adiciona melhores pais aos slots que sobrarem
         QuickSort.ordenarPorFitness(pais);
         for (int i=cont_IndividuosAdicionados; i < CFG.P_QTD_POPULACAO; i++){
         sobreviventes[i] = (Individuo) pais[i-cont_IndividuosAdicionados];
         }
         */
        return sobreviventes;
    }

    private void calculaFitness(Populacao populacao) {
        //Simulacao simulacao = new Simulacao();
        //simulacao.run(populacao, CONFIG_FTNS_GRUPO);
        
        fila.run(populacao);
        QuickSort.ordenarPorFitness(populacao.getIndividuos());
    }

    private void aplicaRechenberg() {
        // Calcula taxas de alteracoes
        float crsv_tx_alteracao = (CFG.RCB_TX_CRSV_MAX - CFG.RCB_TX_CRSV_MIN) /20;
        float mtc_tx_alteracao = (CFG.RCB_TX_MTC_MAX - CFG.RCB_TX_MTC_MIN) / 20;

        if (cont_iteracoesSemEvoluir == 0) {
            // Significa que populacao evoluiu nesta geracao
            ultimasCincoEvolucoes.add(true);
        } else {
            ultimasCincoEvolucoes.add(false);
        }

        // Ajusta crossover
        if (ultimasCincoEvolucoes.size() == 5) {
            int cont_fitnessMelhores = 0;

            //System.out.print("[");
            for (boolean geracaoProgrediu : ultimasCincoEvolucoes) {
                // Percorre a lista dos ultimos 5 'resultados'
                // Se a geracao progrediu incrementa contador
                cont_fitnessMelhores += geracaoProgrediu ? 1 : 0;
                //System.out.print(geracaoProgrediu? " T" : " F");
            }
            //System.out.println(" ] - ");

            if (cont_fitnessMelhores > 1) {
                // Diminui taxas
                if (taxa_crossover >= CFG.RCB_TX_CRSV_MIN + crsv_tx_alteracao) {
                    taxa_crossover -= crsv_tx_alteracao;
                } else {
                    taxa_crossover = CFG.RCB_TX_CRSV_MIN;
                }

                if (taxa_mutacao >= CFG.RCB_TX_MTC_MIN + mtc_tx_alteracao) {
                    taxa_mutacao -= mtc_tx_alteracao;
                } else {
                    taxa_mutacao = CFG.RCB_TX_MTC_MIN;
                }

            } else if (cont_fitnessMelhores == 0) {
                // Aumenta taxas
                if (taxa_crossover <= CFG.RCB_TX_CRSV_MAX - crsv_tx_alteracao) {
                    taxa_crossover += crsv_tx_alteracao;
                } else {
                    taxa_crossover = CFG.RCB_TX_CRSV_MAX;
                }

                if (taxa_mutacao <= CFG.RCB_TX_MTC_MAX - mtc_tx_alteracao) {
                    taxa_mutacao += mtc_tx_alteracao;
                } else {
                    taxa_mutacao = CFG.RCB_TX_MTC_MAX;
                }
            } // Senao (caso tenha aumentado 1 vez, em 5)  não altera taxas

            // Remove ultimo
            ultimasCincoEvolucoes.remove(0);
        }

        if (CFG.CONFIG_DEBUG_LEVEL > 0) {
            System.out.println("Taxa_crossover: " + taxa_crossover
                    + ", Taxa_mutacao: " + taxa_mutacao);
        }
    }
    
    private float getTxMutation(float txAtual){
        Random rnd = new Random();
        float newTx = (float) (rnd.nextGaussian() * 0.35 + txAtual);
        if (newTx < 0){
            newTx = 0;
        }else if(newTx > 1){
            newTx = 1;
        }
        return newTx;
    }
}

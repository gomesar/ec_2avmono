/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import abstratas.AIndividuo;
import principal.AG;

/**
 *
 * @author gomes
 */
public class QuickSort {

    public static void ordenarPorFitness(AIndividuo[] vetor) {
        String tmp = "";
        if (CFG.CONFIG_DEBUG_LEVEL > 2) {
            tmp = "Nao-ordenado: [";
            for (AIndividuo vetor1 : vetor) {
                tmp += " " + vetor1.fitness;
            }
            tmp += "]";
            System.out.println(tmp);
        }

        ordenar(vetor, 0, vetor.length - 1);

        if (CFG.CONFIG_DEBUG_LEVEL > 1) {
            tmp = "Ordenado: [";
            for (AIndividuo vetor1 : vetor) {
                tmp += " " + vetor1.fitness;
            }
            tmp += "]";
            System.out.println(tmp);
        }
    }
    
    private static void ordenar(AIndividuo[] vetor, int inicio, int fim) {
        if (inicio < fim) {
            int posicaoPivo = separar(vetor, inicio, fim);
            ordenar(vetor, inicio, posicaoPivo - 1);
            ordenar(vetor, posicaoPivo + 1, fim);
        }
    }

    private static int separar(AIndividuo[] vetor, int inicio, int fim) {
        AIndividuo pivo = vetor[inicio];
        int i = inicio + 1, f = fim;
        while (i <= f) {
            if (vetor[i].fitness >= pivo.fitness) {
                i++;
            } else if (pivo.fitness > vetor[f].fitness) {
                f--;
            } else {
                AIndividuo troca = vetor[i];
                vetor[i] = vetor[f];
                vetor[f] = troca;
                i++;
                f--;
            }
        }
        vetor[inicio] = vetor[f];
        vetor[f] = pivo;
        return f;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

/**
 *
 * @author gomes
 */
public final class CFG {

    public static int /* Variavel para auxiliar na debugacao do codigo, suas funcoes sao
             * acumulativos
             * 0 - Desligado
             * 1 - Printa informacoes de fluxo
             * 2 - Printa informacoes de valores e iteracoes
             * 3 - !CUIDADO! Executa prints detalhados que podem estar dentro de lacos
             * repetidos muitas vezes.
             */ CONFIG_DEBUG_LEVEL = 0,
            // Condicao de parada, em modo debugacao diminui as iteracoes
            CONFIG_CP_MAX_ITERACOES = CONFIG_DEBUG_LEVEL > 0 ? 10 : 100;

    // FILA ## NOVO SIMULADOR
    public static final int FILA_DIAS_POR_RODADA = 5,
            FILA_TESTES_POR_DIA = 5,
            FILA_AMIGOS_NA_FILA = 4;
            
    // **************************** SIMULADOR ***********************************
    public static final float SIM_TX_SER_AMIGO = 0.05f,
            SIM_TX_BONUS = 1.5f,
            SIM_TX_VERGONHA = 0.5f;
    public static final int SIM_TEMPO_FECHADO = 5;
    // ************************* INDIVIDUOS ********************************
    public static final int // Intervalo de chegada na fila (Ex: de 11:30h ate 14:29h = 180 minutos)
            I_INTERVALO_CHEGADA = 20 + SIM_TEMPO_FECHADO,
            I_QTD_GENES = 15,
            I_QTD_OBJETIVOS = 2;

        //
    // ************************* POPULACAO ********************************
    public static final int P_QTD_POPULACAO = 100;

    // **************************** AG ***********************************
    private static final float CONFIG_SS_PROCENT_FILHOS = 0.1f;
    public static final int // 4 semanas no mes * 12 meses no ano * 10 anos
            AG_CP_MAX_ITERACOES = 4 * 12 * 20,
            AG_CP_MAX_ITERACOES_SEM_EVOLUCAO = 4 * 3 ,
            AG_SS_QTD_FILHOS = Math.round(P_QTD_POPULACAO * CONFIG_SS_PROCENT_FILHOS), // PRECISA SER PAR!!
            AG_TORN_QTD_INDIV_RINGUE = 4;
            
    public static final float AG_MR_TX_MATAR_PAI = 0.6f,
            AG_MAX_FAIXA_FITNESS = 0.4f;

    // Rechemberg
    public static final float RCB_TX_CRSV_MAX = 0.9f,
            RCB_TX_CRSV_MIN = 0.25f,
            RCB_TX_MTC_MAX = 0.3f,
            RCB_TX_MTC_MIN = 0.09f;

}

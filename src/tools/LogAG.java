/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import abstratas.AAG;
import abstratas.APopulacao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import principal.Populacao;

/**
 *
 * @author gomes
 */
public class LogAG {

    private PrintWriter writer;
    private File file;

    public LogAG(String nomeAG) {
        try {
            // Cria arquivo em branco com o nome passado
            file = new File(nomeAG+".csv");
            FileWriter fw;
            fw = new FileWriter(file, false);
            writer = new PrintWriter(fw);
        } catch (IOException ex) {
            Logger.getLogger(LogAG.class.getName()).log(Level.SEVERE, null, ex);
        }

        //writer = new PrintWriter("EC_atv02.csv", "UTF-8");
        
        // Escreve cabeçalho (nomes das colunas da matriz)
        writer.println("Iteracao;MelhorFitness;Media;DesvioPadrao;MediaTxFuro;MediaTxReclame;RodadasSE");

    }

    public void gravaIteracao(AAG AG, APopulacao p) {
        NumberFormat format_taxa = new DecimalFormat("0.000");
        NumberFormat format_count = new DecimalFormat("000");
        
        Populacao l_pop = (Populacao) p;
        
        String linha = 
                format_count.format(AG.getNumIteracao()) + ";"
                + format_taxa.format(p.getMelhorIndividuo().fitness) + ";"
                + format_taxa.format(p.getMedia()) + ";"
                + format_taxa.format(p.getDesvioPadrao())+ ";"
                + format_taxa.format(l_pop.getMediaTxFuro()) + ";"
                + format_taxa.format(l_pop.getMediaTxReclame()) + ";"
                + format_count.format(AG.getRodadasSemEvoluir()) + ";";
        writer.println(linha);
    }
    
    public void encerra(String melhorIndividuo){
        // Escreve melhor individuo e fecha arquivo
        writer.println("Melhor Individuo: " + melhorIndividuo);
        
        writer.close();
    }
}

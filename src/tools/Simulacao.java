/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import principal.Individuo;
import principal.Populacao;

/**
 *
 * @author gomes
 */
public class Simulacao {

    private final int ATENDIMENTOS_POR_MINUTO = 4;
    private final int ST_CHEGANDO = 0, ST_NA_FILA = 1, ST_ATENDIDO = 2;
    private final String[] dias = {"SEG", "TER", "QUAR", "QUIN", "SEXT"};
    private final int[] index_Hora = {2, 5, 8, 11, 14};
    private List<Cliente> clientes, fila, atendidos;
    private MersenneTwisterFast tw;
    protected int dia;
    
    private int CNT_FUROS, CNT_RECLAMES;

    public void run(Populacao populacao, boolean fitnessEmGrupo) {
        CNT_FUROS=0;
        CNT_RECLAMES=0;
        tw = new MersenneTwisterFast();
        
        clientes = new ArrayList<>();
        for (int index = 0; index < CFG.P_QTD_POPULACAO; index++) {
            Cliente novoCliente = new Cliente ( (Individuo) populacao.getIndividuo(index) );
            novoCliente.index = index;
            clientes.add(novoCliente);
            //System.out.println("[" + index +"] " +novoCliente.indv.cromossomo[0] + 
              //      "," +novoCliente.indv.cromossomo[1] + "," + novoCliente.indv.cromossomo[2]);
        }

        for (dia=0; dia < index_Hora.length; dia++) {
            // Retorna todos ao status de "chegando"
            for (Cliente cliente : clientes) {
                cliente.restart();
            }
            //Collections.sort(clientes);
            
            /*
            for (int test=0; test < clientes.size(); test++) {
                System.out.println("["+test+"]:"+clientes.get(test).indv.cromossomo[index_Hora[dia]] + ", ");
            }*/
            // Zera variaveis
            fila = new ArrayList<>();
            atendidos = new ArrayList<>();
            int clientesAtendidos = 0;
            int tempoAtual = 0;
            if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("\t!--" + dias[dia]+"---");

            while (clientesAtendidos < clientes.size()) {
                if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.print("![TEMPO " + tempoAtual +"] ");
                
                // 1. Chegada
                for (Cliente cliente : clientes) {
                    if (cliente.status == ST_CHEGANDO && cliente.indv.cromossomo[index_Hora[dia]] <= tempoAtual) {
                        //System.out.println(cliente.indv.cromossomo[semana[dia]] + " <= " + tempoAtual);
                        
                        for (int i_fila = 0 ; i_fila < fila.size()-1; i_fila++) {
                            // Se tem amigo
                            
                            if (tw.nextFloat() <= CFG.SIM_TX_SER_AMIGO){
                                //System.out.println("\n@"+cliente.index+": Encontrei um amigo..");
                                // Verifica se fura
                                float taxaFuro = (float) (fila.size() - i_fila)/fila.size();
                                if (cliente.indv.cromossomo[index_Hora[dia]-2] >= taxaFuro){
                                    if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("@"+cliente.index+": Fala "+ fila.get(i_fila).index +"!");
                                
                                    // Verifica se reclamam ( no coletivo)
                                    float taxaReclame = (float) i_fila/fila.size();
                                    if (fitnessEmGrupo && fila.get(i_fila+1).indv.cromossomo[index_Hora[dia]-1] >= taxaReclame){
                                        if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("@"+fila.get(i_fila+1).index+": FURÃO!");
                                        fila.get(i_fila+1).reclamou[dia] = true;
                                        cliente.passouVergonha[dia] = true;
                                        CNT_RECLAMES++;
                                    } else {
                                        // Se ninguem reclamar, fura (entra logo apos o amigo)
                                        fila.add(i_fila+1, cliente);
                                        cliente.furou[dia] = true;
                                        cliente.debuff[dia] = 1 - taxaFuro;
                                        if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("@"+cliente.index+": FUREI!");
                                        CNT_FUROS++;
                                        break;
                                    }
                                }
                                
                            }
                        
                        }
                        if (cliente.furou[dia] == false){
                            fila.add(cliente);
                        }
                        cliente.status = ST_NA_FILA;
                    }
                }

                // 2. Atualiza tempo de espera
                if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.print("FILA");
                for (Cliente clienteNaFila : fila) {
                    clienteNaFila.tempoEspera[dia]++;
                    if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.print( ": " + clienteNaFila.index);
                }
                if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("");
                
                
                /*
                 // Atualiza atendimentos disponiveis
                 for (int i = 0; i < tempoAtendimento.length; i++) {
                 if (tempoAtendimento[i] > 0) {
                 tempoAtendimento[i]--;

                 if (tempoAtendimento[i] == 0) {
                 atendDisp++;
                 }
                 }
                 }*/
                
                // 3. Atendimento
                if (tempoAtual >= CFG.SIM_TEMPO_FECHADO) {
                    for (int atend = 0; atend < ATENDIMENTOS_POR_MINUTO; atend++) {
                        if (!fila.isEmpty()){
                            fila.get(0).status = ST_ATENDIDO;
                            fila.remove(0);
                            clientesAtendidos++;
                        }
                    }
                }

                if (CFG.CONFIG_DEBUG_LEVEL > 0) System.out.println("\t(" + clientesAtendidos +"/"+ clientes.size()+")");
                tempoAtual++;
            }
        }

        // Calcula fitness
        boolean DEBUG_FITNESS = false;
        if (fitnessEmGrupo == false){
            for (Cliente cliente : clientes) {
                float soma =0;
                for (int i = 0; i < cliente.tempoEspera.length; i++) {
                    soma += cliente.tempoEspera[i];
                }

                cliente.indv.fitness = 2f / (soma / cliente.tempoEspera.length);
                //System.out.println("F: " + cliente.indv.fitness);
            }
        }else{
            //System.out.print("\n::::::::::::::::::::::::::::::::::");
            for (Cliente cliente : clientes) {
                float soma =0;
                for (int i = 0; i < cliente.tempoEspera.length; i++) {
                    soma += cliente.tempoEspera[i];
                }

                cliente.indv.fitness = 2f / (soma / cliente.tempoEspera.length);
                
                if(DEBUG_FITNESS) System.out.print("\nFT: " + cliente.indv.fitness);
                if (cliente.getMediaDebuff() != 0){ // Se furou alguma vez
                    cliente.indv.fitness *= cliente.getMediaDebuff();
                    //cliente.indv.fitness *= 0.9;
                    if(DEBUG_FITNESS) System.out.print(" {FR: " + cliente.indv.fitness+"}");
                }
                for (int i=0; i <5 ;i++){
                    if (cliente.passouVergonha[i]){
                        cliente.indv.fitness *= 1- CFG.SIM_TX_VERGONHA;
                        //cliente.indv.fitness *= 0.9;
                        if(DEBUG_FITNESS) System.out.print(" {PV: " + cliente.indv.fitness+"}");
                    }
                    if (cliente.reclamou[i]){
                        cliente.indv.fitness *= CFG.SIM_TX_BONUS;
                        if(DEBUG_FITNESS) System.out.print(" {RC: " + cliente.indv.fitness+"}");
                        if (cliente.indv.fitness > 1f){
                            //cliente.indv.fitness = 1f;
                            //System.out.println("!!");
                        }
                    }
                }
                //System.out.println("F: " + cliente.indv.fitness);
            }
        }
        
        //System.out.println("\tFUROS: " + CNT_FUROS + " , RECLAMES: " + CNT_RECLAMES);
    }
    
    

    private class Cliente implements Comparable<Cliente>{
        protected Individuo indv;
        protected int status, index;
        protected int[] tempoEspera;
        protected boolean[] reclamou, furou, passouVergonha;
        protected float[] debuff;
        

        public Cliente(Individuo individuo) {
            this.indv = individuo;
            this.status = ST_CHEGANDO;
            this.tempoEspera = new int[index_Hora.length];
            this.index = -1;
            this.reclamou = new boolean[5];
            this.furou = new boolean[5];
            this.passouVergonha = new boolean[5];
            this.debuff = new float[5];
            /*
            for (int i = 0; i < 5; i++) {
                this.reclamou[i]= false;
                this.furou[i] = false;
                this.passouVergonha[i] = false;
                this.debuff[i] = 0f;
                
            }
            */
        }

        public void restart(){
            this.status = ST_CHEGANDO;
        }
        
        public float getMediaDebuff(){
            float media = 0f;
            int cont = 0;
            for (int i=0; i < furou.length; i++) {
                if(furou[i]){
                    media += debuff[i];
                    cont++;
                }
            }
            return cont==0 ? 0 : media / 5;
        }

        @Override
        public int compareTo(Cliente t) {
            int result = 0;
            float comparacao = this.indv.cromossomo[index_Hora[dia]] - t.indv.cromossomo[index_Hora[dia]];
            if (comparacao < 0){
                result = -1;
            }else if (comparacao > 0){
                result = 1;
            }
            return result;
        }
    }
}

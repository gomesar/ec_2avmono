/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstratas;

import tools.CFG;

/**
 *
 * @author gomes
 */
public abstract class AIndividuo implements Comparable<AIndividuo>{
    public int idade;
    public float fitness;
    private int paretoFront;
    private double CrowdingDistance;
    private double objective[];

    public AIndividuo(){
        this.fitness = 0f;
        this.idade = 0;
        this.paretoFront = 0; // 0? 1? 100 ? infinito?
        this.CrowdingDistance = 0d;
        this.objective = new double[CFG.I_QTD_OBJETIVOS];
        for (int i = 0; i < objective.length; i++) {
            objective[i] = 0.2d;
        }
    }
    
    public abstract void inicializa();
    
    public abstract int getGrauSemelhanca(AIndividuo comparado);
    
    public abstract AIndividuo getCopia();
    
    /**
     *
     * @param q
     * @return
     */
    public boolean dominates(AIndividuo q){
        boolean dominates = false;
        for (int i = 0; i < objective.length; i++) {
            // Se [P] domina [Q] em um objetivo
            if (this.objective[i] > q.objective[i]){
                // Seta dominancia para true e continua testando objetivos
                dominates = true;
            }else // Senao, se for pior retorna falso
            if (this.objective[i] < q.objective[i]){
                return false;
            }
            // Se for igual, mantem a dominancia atual
            // False se nao dominar em nenhum outro objetivo
            // E true se ja tiver algum objetivo melhor que [Q]
        }
        return dominates;
    };
    
    public void setParetoFront(int indice){
        this.paretoFront = indice;
    };

    public void setCrowdingDistance(double crwDistance) {
        this.CrowdingDistance = crwDistance;
    }

    public void setObjective(int i, double value) {
        this.objective[i] = value;
    }
    
    public int getParetoFront() {
        return paretoFront;
    }

    public double getCrowdingDistance() {
        return CrowdingDistance;
    }

    public double getObjective(int indiceObjetivo) {
        return this.objective[indiceObjetivo];
    }
    
    public int compareTo(AIndividuo outroIndividuo){
        /* Menor primeiro 
        EX: this=> 1, outro=> 3
            1-3 = -2
        ou seja, negativo significa que 'this' vem antes que outro
        */
        return this.paretoFront - outroIndividuo.getParetoFront();
    }

    public abstract void calculaAtributos();
}

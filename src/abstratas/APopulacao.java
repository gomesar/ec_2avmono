/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstratas;


/**
 *
 * @author gomes
 */
public abstract class APopulacao {
    protected AIndividuo[] individuos;
    protected float media, desvioPadrao;
    protected int id_melhorIndividuo;
    
    public APopulacao(int qtd_individuos){
        this.individuos = new AIndividuo[qtd_individuos];
        this.media = 0f;
        this.desvioPadrao = 0f;
        this.id_melhorIndividuo = 0;
    }

    public AIndividuo getMelhorIndividuo(){
        return individuos[id_melhorIndividuo];
    }
    
    public AIndividuo getIndividuo(int index){
        // Abstraindo implementaçao, List<> ou vetor
        return individuos[index];
    }
    
    public AIndividuo getCopiaIndividuo(int index){
        return individuos[index].getCopia();
    };

    public float getMedia() {
        return this.media;
    }

    public float getDesvioPadrao() {
        return this.desvioPadrao;
    }
    
    public AIndividuo[] getIndividuos() {
        return individuos;
    }

    public void setIndividuos(AIndividuo[] individuos) {
        this.individuos = individuos;
    }

    public void setId_melhorIndividuo(int id_melhorIndividuo) {
        this.id_melhorIndividuo = id_melhorIndividuo;
    }
    
    public static void calculateAttributes(AIndividuo[] individuos) {
        for (int i = 0; i < individuos.length; i++) {
            individuos[i].calculaAtributos();
        }
    }
}
